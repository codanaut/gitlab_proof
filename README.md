# Gitlab Proof For Keyoxide

This is an OpenPGP proof that connects my OpenPGP key to this GitLab account.

For details check out https://keyoxide.org/guides/openpgp-proofs

My Keyoxide profile: https://keyoxide.org/hkp/270A6411B1B5F57E03D6FAE5623F9AD1C7C17D4F